//require('dotenv').config();
//require('dotenv').config();
require('dotenv').config();

module.exports = {
	client: 'pg',
	connection: process.env.DATABASE_URL,
	pool: {
		min: 2,
		max: 10,
	},
};
