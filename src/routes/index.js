const express = require('express');

const itemListController = require('../controller/itemList.controller');
const userController = require('../controller/users.controller');
const sessionController = require('../controller/session.controller');
const requireUser = require('../middleware/requireUser');

const router = express.Router();

router.post('/itemList', itemListController.createItemList);
router.post('/itemListg', itemListController.getItemsList);
router.delete('/:id', itemListController.delItem);
router.patch('/:id', itemListController.patchItem);
//router.put('/:id', itemListController.putItem);
router.post('/signup', userController.register);
router.post('/login', userController.login);
router.post('/getUserInfo', userController.getUserInfo);
//router.get('/getAccountInfo', userController.getAccountInfo);
router.post('/api/getOwnItems', requireUser.requireUser, itemListController.getOwnItems);
router.put('/api/:id', userController.putUserInfo);

router.post('/api/createuser', userController.accountLinks);
router.post('/api/checkout', userController.checkOut);

router.post('/api/session', sessionController.createSessionHandler);

router.post('/api/stripeAccount', userController.stripeAccountObject);

//router.delete('/api/session', sessionController.deleteSessionHandler);
router.post('/api/sessiong', requireUser.requireUser, sessionController.getSessionHandler);
router.delete('/api/session', requireUser.requireUser, sessionController.deleteSessionHandler);

module.exports = router;
