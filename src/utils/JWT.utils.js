const jwt = require('jsonwebtoken');
require('dotenv').config();

const publicKey = JSON.parse(process.env.PUBLIC_KEY);

const privateKey = JSON.parse(process.env.PRIVATE_KEY);

class verifyJWT {
	signJWT(payload, expiresIn) {
		return jwt.sign(payload, privateKey, { algorithm: 'RS256', expiresIn });
	}
	verifyJWT(token) {
		try {
			const decoded = jwt.verify(token, publicKey);
			return { payload: decoded, expired: false };
		} catch (error) {
			return { payload: null, expired: error.message.includes('jwt expired') };
		}
	}
}
module.exports = new verifyJWT();
