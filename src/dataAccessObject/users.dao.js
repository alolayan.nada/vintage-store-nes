const db = require('../db/db.js');

class userDAO {
	async register(email, hashedPassword, first_name, last_name, stripe_id, stripe_id_setup_url) {
		try {
			const email_exists = await db
				.select()
				.table('users')
				.where('email', email)
				.then((res) => res);
			if (email_exists.length) {
				return res.status(400).json('A user with this email already exists');
			} else {
				const [id] = await db
					.into('users')
					.insert({
						first_name: first_name,
						last_name: last_name,
						email: email,
						password: hashedPassword,
						stripe_id: stripe_id,
						stripe_id_setup_url: stripe_id_setup_url,
					})
					.returning('id');
				const user = this.getUserInfo(email);

				return user;
			}
		} catch {}
	}

	async login(email, password) {
		try {
			const user = await db
				.select()
				.table('users')
				.where('email', email)
				.then((res) => res);
			return { user: user, password: password };
		} catch {}
	}

	async patchUserInfo(id, column) {
		await db('users').where({ id: id }).update(column).returning('*');
		const itemsList = await db.select().table('users');
		return itemsList;
	}

	async putUserInfo(stripe_id, user_id) {
		await db('users').where({ id: user_id }).update({ stripe_id: stripe_id });
		const itemsList = await db.select().table('users');
		return itemsList;
	}

	async getUserInfo(email) {
		try {
			const user = await db
				.select()
				.table('users')
				.where('email', email)
				.then((res) => res);
			return user;
		} catch {}
	}
}

module.exports = new userDAO();
