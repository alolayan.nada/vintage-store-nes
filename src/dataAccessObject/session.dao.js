const db = require('../db/db.js');

class sessionDAO {
	async getSession(sessionId) {
		const [session] = await db.select().table('sessions').where({ session_id: sessionId });
		return session && session.valid ? session : null;
	}

	async invalidateSession(sessionId) {
		const [session] = await db.select().table('sessions').where({ session_id: sessionId });

		if (session) {
			const [id] = await db
				.select()
				.table('sessions')
				.where({ session_id: sessionId })
				.update({ valid: false })
				.returning('session_id');

			const session2 = await db.select().table('sessions').where({ session_id: sessionId });

			return session;
		}
	}

	//add name?
	async createSession(email, name) {
		const [id] = await db
			.into('sessions')
			.returning(['email', 'valid', 'name'])
			.insert({
				email: email,
				valid: true,
				name: name,
			})
			.returning('session_id');
		const items = await db
			.select()
			.table('items_list')
			.then((res) => console.log(res));

		const { session_id } = id;

		const [session] = await db.select().table('sessions').where({ session_id: session_id });

		return session && session.valid ? session : 'session wasnt created';
	}
}

module.exports = new sessionDAO();
