const db = require('../db/db.js');

class itemListDAO {
	async addItem(
		imageUrl,
		price,
		title,
		condition,
		seller_name,
		seller_stripe_id,
		stripe_id_setup_url
	) {
		// try {
		const [id] = await db
			.into('items_list')
			.returning([
				'buyer_address',
				'buyer_family_name',
				'buyer_name',
				'buyer_phone',
				'buyer_postal_code',
				'game_condition',
				'game_console',
				'game_price',
				'game_publisher',
				'game_title',
				'image_URL',
				'movie_URL',
				'seller_address',
				'seller_family_name',
				'seller_name',
				'seller_phone',
				'seller_postal_code',
				'seller_stripe_id',
			])
			.insert({
				image_URL: imageUrl,
				game_price: price,
				game_title: title,
				game_condition: condition,
				seller_name: seller_name,
				seller_stripe_id: seller_stripe_id,
			})
			.returning('id');
		return id;
	}

	async getAllItems() {
		try {
			const items = await db
				.select()
				.table('items_list')
				.then((res) => res);

			return items;
		} catch {}
	}
	async delItem(id) {
		await db('items_list').where({ id: id }).delete();
		const itemsList = await db.select().table('items_list');
		return itemsList;
	}

	async patchItem(id, postData) {
		await db('items_list').where({ id: id }).update(postData).returning('*');
		const itemsList = await db.select().table('items_list');
		return itemsList;
	}
	//
	async putItem(
		id,
		buyer_address,
		buyer_family_name,
		buyer_name,
		buyer_phone,
		buyer_postal_code,
		game_condition,
		game_console,
		game_price,
		game_publisher,
		game_title,
		image_URL,
		seller_address,
		movie_URL,
		seller_family_name,
		seller_name,
		seller_phone,
		seller_postal_code
	) {
		await db('items_list').where({ id: id }).update({ buyer_address: buyer_address });
		await db('items_list').where({ id: id }).update({ buyer_family_name: buyer_family_name });
		await db('items_list').where({ id: id }).update({ buyer_name: buyer_name });
		await db('items_list').where({ id: id }).update({ buyer_phone: buyer_phone });
		await db('items_list').where({ id: id }).update({ buyer_postal_code: buyer_postal_code });
		await db('items_list').where({ id: id }).update({ game_condition: game_condition });
		await db('items_list').where({ id: id }).update({ game_console: game_console });
		await db('items_list').where({ id: id }).update({ game_price: game_price });
		await db('items_list').where({ id: id }).update({ game_publisher: game_publisher });
		await db('items_list').where({ id: id }).update({ game_title: game_title });
		await db('items_list').where({ id: id }).update({ image_URL: image_URL });
		await db('items_list').where({ id: id }).update({ movie_URL: movie_URL });
		await db('items_list').where({ id: id }).update({ seller_address: seller_address });
		await db('items_list').where({ id: id }).update({ seller_family_name: seller_family_name });
		await db('items_list').where({ id: id }).update({ seller_name: seller_name });
		await db('items_list').where({ id: id }).update({ seller_phone: seller_phone });
		await db('items_list').where({ id: id }).update({ seller_postal_code: seller_postal_code });
		const itemsList = await db.select().table('items_list');
		return itemsList;
	}
	async getOwnItem(seller_email) {
		//seller_name and email are unique , the names have to be changed

		const items = await db('items_list').where({ seller_name: seller_email });
		return items;
	}
}
module.exports = new itemListDAO();
