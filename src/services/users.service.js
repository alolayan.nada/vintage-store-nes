const usersDAO = require('../dataAccessObject/users.dao');
const bcrypt = require('bcryptjs');

class userService {
	register(postData) {
		const { email, password, first_name, last_name, stripe_id, stripe_id_setup_url } = postData;
		const hashedPassword = bcrypt.hashSync(password, 12);

		return usersDAO.register(
			email,
			hashedPassword,
			first_name,
			last_name,
			stripe_id,
			stripe_id_setup_url
		);
	}

	async login(postData) {
		const { email, password } = postData;

		const user = await usersDAO.login(email, password);

		return user;
	}

	async getUserInfo(postData) {
		const { email } = postData;

		return usersDAO.getUserInfo(email);
	}

	async patchUserInfo(postData) {
		const { id, column } = postData;

		return usersDAO.patchUserInfo(id, column);
	}
	async putUserInfo(stripe_id, user_id) {
		return usersDAO.putUserInfo(stripe_id, user_id);
	}
}
module.exports = new userService();
