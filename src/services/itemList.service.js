const itemListDAO = require('../dataAccessObject/itemList.dao');

class itemListService {
	createItemList(itemList) {
		const { image_URL, game_price, game_title, game_condition, seller_name } = itemList;

		return itemListDAO.addItem(image_URL, game_price, game_title, game_condition, seller_name);
	}
	getAllItems() {
		return itemListDAO.getAllItems();
	}
	delItem(id) {
		return itemListDAO.delItem(id);
	}
	getOwnItem(seller_email) {
		return itemListDAO.getOwnItem(seller_email);
	}

	patchItem(id, post) {
		const postdata = post;
		return itemListDAO.patchItem(id, postdata);
	}
	putItem(id, post) {}
}

module.exports = new itemListService();
