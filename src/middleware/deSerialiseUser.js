const { NextFunction, Request, Response } = require('express');
const getSession = require('../dataAccessObject/session.dao');
const { signJWT, verifyJWT } = require('../utils/JWT.utils');

class deserializeUser {
	async deserializeUser(req, res, next) {
		const { accessToken, refreshToken } = req.cookies;

		if (!accessToken) {
			return next();
		}

		const { payload, expired } = verifyJWT(accessToken);

		if (payload) {
			req.user = payload;
			return next();
		}

		const { payload: refresh } =
			expired && refreshToken ? verifyJWT(refreshToken) : { payload: null };

		if (!refresh) {
			return next();
		}

		const session = await getSession.getSession(refresh.sessionId);

		if (!session) {
			return next();
		}

		const newAccessToken = signJWT(session, '5s');

		res.cookie('accessToken', newAccessToken, {
			maxAge: 300000, // 5 minutes
			httpOnly: true,
		});

		// @ts-ignore
		req.user = verifyJWT(newAccessToken).payload;

		return next();
	}
}

module.exports = new deserializeUser();
