const itemListService = require('../services/itemList.service');
const db = require('../db/db.js');

class itemListController {
	async createItemList(req, res) {
		try {
			const id = await itemListService.createItemList(req.body);
			console.log(id);
			res.status(201).json([id]);
		} catch (error) {
			console.log(error);
			res.status(500).json('validation');
			//
		}
	}

	async getItemsList(req, res) {
		try {
			const items = await db
				.select()
				.table('items_list')
				.then((res) => res);

			res.status(201).json(items);
		} catch {}
	}

	async delItem(req, res) {
		try {
			const id = await itemListService.delItem(req.params.id);
			res.status(201).json(id);
		} catch (error) {
			console.log(error);
			res.status(500).json('validation');
		}
	}

	async getOwnItems(req, res) {
		try {
			const items = await itemListService.getOwnItem(req.user.email);
			res.status(201).json(items);
		} catch (error) {
			console.log(error);
			res.status(500).json('validation');
		}
	}

	async patchItem(req, res) {
		try {
			const id = await itemListService.patchItem(req.params.id, req.body);
			res.status(201).json(id);
		} catch (error) {
			console.log(error);
			res.status(500).json('validation');
		}
	}
	async putItem(req, res) {}
}

module.exports = new itemListController();
