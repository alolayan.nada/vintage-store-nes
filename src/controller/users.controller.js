const userService = require('../services/users.service');
require('dotenv').config();

const stripe = require('stripe')(process.env.STRIPE_S_KEY);

class userController {
	async register(req, res) {
		try {
			const id = await userService.register(req.body);
			res.status(201).json(id);
		} catch (error) {
			console.log(error);
			res.status(500).json('validation');
		}
	}
	//
	async accountLinks(req, res) {
		try {
			const account = await stripe.accounts.create({
				country: 'JP',
				type: 'express',
				capabilities: {
					card_payments: {
						requested: true,
					},
					transfers: {
						requested: true,
					},
				},
			});

			const accountLinks = await stripe.accountLinks.create({
				account: account.id,
				refresh_url: 'https://dashboard.stripe.com',
				return_url: 'https://vintage-store-nes.herokuapp.com/',
				type: 'account_onboarding',
			});

			return res.send({ url: accountLinks.url, account_id: account.id });
		} catch (error) {
			console.log(error);
			res.status(500).json('validation');
		}
	}

	async checkOut(req, res) {
		try {
			const session = await stripe.checkout.sessions.create({
				payment_method_types: ['card'],
				line_items: [
					{
						price_data: {
							currency: 'jpy',
							product_data: {
								name: req.body.name,
								images: [`${req.body.img}`],
							},
							unit_amount: req.body.price,
						},
						quantity: 1,
					},
				],
				payment_intent_data: {
					application_fee_amount: 123,
					transfer_data: {
						destination: req.body.seller_id,
					},
				},
				mode: 'payment',
				success_url: `https://dashboard.stripe.com`,
				cancel_url: `https://vintage-store-nes.herokuapp.com/`,
			});
			return res.send(session);
		} catch (error) {
			console.log(error);
			res.status(500).json('validation');
		}
	}

	async patchUserInfo(req, res) {
		try {
			console.log('in put user info!!!##');
			const user = await userService.patchUserInfo(req.body);
			res.status(201).json(user);
		} catch (error) {
			console.log(error);
			res.status(500).json('validation');
		}
	}
	async putUserInfo(req, res) {
		try {
			const user = await userService.putUserInfo(req.body.stripeId, req.params.id);
			res.status(201).json(user);
		} catch (error) {
			console.log(error);
			res.status(500).json('validation');
		}
	}

	async getUserInfo(req, res) {
		try {
			const user = await userService.getUserInfo(req.body);
			res.status(201).json(user);
		} catch (error) {
			console.log(error);
			res.status(500).json('validation');
		}
	}

	async stripeAccountObject(req, res) {
		try {
			const account = await stripe.accounts.retrieve(req.body.account_id);
			return res.send(account);
		} catch (error) {
			console.log(error);
		}
	}

	async login(req, res) {}
}
module.exports = new userController();
