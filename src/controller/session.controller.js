const userService = require('../services/users.service');
const { signJWT, verifyJWT } = require('../utils/JWT.utils');
const { createSession, invalidateSession } = require('../dataAccessObject/session.dao');
const moment = require('moment');

require('dotenv').config();

const bcrypt = require('bcryptjs');

class sessionController {
	async createSessionHandler(req, res) {
		try {
			const user = await userService.login(req.body);

			console.log('Existing user in login');

			if (user.user[0]) {
				const authUser = await bcrypt
					.compare(user.password, user.user[0].password)
					.then((isMatch) => {
						console.log('Check isMatch', isMatch);
						if (isMatch) {
							(async () => {
								const session = await createSession(user.user[0].email, user.user[0].first_name);

								const accessToken = signJWT(
									{
										email: session.email,
										name: session.name,
										sessionId: session.session_id,
									},
									'5s'
								);
								const refreshToken = signJWT({ sessionId: session.session_id }, '1y');

								res.cookie('accessToken', accessToken, {
									maxAge: 300000, // 5 minutes
									httpOnly: true,
								});

								res.cookie('refreshToken', refreshToken, {
									maxAge: 3.154e10, // 1 year
									httpOnly: true,
								});

								return res.send(session);
							})();
						} else {
							return res.json('Password Incorrect');
						}
					});
				return authUser;
			} else {
				return res.json('Email not registered');
			}
		} catch (error) {
			console.log(error);
			res.status(500).json('validation');
		}
	}

	async getSessionHandler(req, res) {
		try {
			const exp = new Date(req.user.exp * 1000);
			const iat = new Date(req.user.iat * 1000);
			const fexp = moment(exp).format('dd,YYYY,h:mma:ss');
			const fiat = moment(iat).format('dd,YYYY,h:mma:ss');

			res.send({ user: req.user, iat: fiat, exp: fexp });
		} catch (error) {
			res.status(500).json('validation');
		}
	}

	async deleteSessionHandler(req, res) {
		try {
			res.cookie('accessToken', '', {
				maxAge: 0,
				httpOnly: true,
			});

			res.cookie('refreshToken', '', {
				maxAge: 0,
				httpOnly: true,
			});
			// @ts-ignore
			const session = invalidateSession(req.user.session_id);
			return res.send(session);
		} catch (error) {}
	}
}
module.exports = new sessionController();
