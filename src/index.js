require('dotenv').config();
const express = require('express');
const router = require('./routes');
const deserializeUser = require('./middleware/deSerialiseUser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const path = require('path');
const app = express();

app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

const port = process.env.PORT || 4000;

app.use(
	cors({
		credentials: true,
		origin: 'http://localhost:3000/',
	})
);

app.use(deserializeUser.deserializeUser);

if (process.env.DB_ENVIRONMENT === 'production') {
	// Serve any static files
	app.use(express.static(path.join(__dirname, '../front-end/build')));

	// Handle React routing, return all requests to React app
	app.get('*', function (req, res) {
		res.sendFile(path.join(__dirname, '../front-end', 'build/', 'index.html'));
	});
}
app.use(router);
app.listen(port, () => {
	console.log(`🚀 Server is running on ${port}`);
});
