/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
	return knex.schema.alterTable('users', (table) => {
		table.string('stripe_id_setup_url', 128);
	});
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
	return knex.schema.alterTable('users', (table) => {
		table.dropColumn('stripe_id_setup_url');
	});
};
