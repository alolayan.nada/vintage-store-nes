/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
	return knex.schema.createTable('sessions', (table) => {
		table.increments('session_id').index();
		table.string('name').notNullable();
		table.string('email').notNullable();
		table.boolean('valid').notNullable();
	});
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
	knex.schema.dropTableIfExists('sessions');
};
