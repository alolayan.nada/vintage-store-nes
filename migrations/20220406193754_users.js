/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
	return knex.schema.createTable('users', (table) => {
		table.increments().index();
		table.text('first_name').notNullable();
		table.text('last_name').notNullable();
		table.text('email').notNullable();
		table.text('password').notNullable();
		table.text('stripe_id');
	});
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
	knex.schema.dropTableIfExists('users');
};
