module.exports = {
	client: 'pg',
	debug: true,
	connection: process.env.DATABASE_URL,

	migrations: {
		tableName: 'knex_migrations',
		directory: __dirname + '/migrations',
	},
	seeds: {
		directory: __dirname + '/seeds',
	},
	ssl: true,
};
