import React, { useContext, createContext } from 'react';
import axios from 'axios';

const authContext = createContext();

export function AuthProvider({ children }) {
	const auth = useProvideAuth();
	return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

export const useAuth = () => {
	return useContext(authContext);
};

function useProvideAuth() {
	const [userfromAuth, setUserformAuth] = React.useState(null);
	const [loading, setLoading] = React.useState(true);
	const [authed, setAuthed] = React.useState(false);

	async function handleUser(rawUser) {
		await (() => {
			if (rawUser) {
				setLoading(false);
				setAuthed(true);
				setUserformAuth(rawUser);

				return rawUser;
			} else {
				setLoading(false);
				setUserformAuth(false);
				setAuthed(false);

				return false;
			}
		})();
	}

	async function login(email, password) {
		const user = await axios
			.post('/api/session', {
				email: email,
				password: password,
			})
			.then((res) => {
				if (res.data.name) {
					handleUser(res.data);
					setAuthed(true);

					return res.data;
				}
			})
			.catch((err) => {});
		return user;
	}

	async function signout() {
		const user = await axios
			.delete('/api/session', { withCredentials: true })
			.then((res) => {
				handleUser(res.data);
				setAuthed(false);
			})
			.catch((error) => console.log(error, 'from delete!!!#'));

		return user;
	}
	async function getAuth() {
		return authed;
	}

	async function checkSessionData() {
		const check = await axios
			.post('/api/sessiong')
			.then((res) => {
				setAuthed(true);
				if (res) return true;
			})
			.catch((error) => console.log(error));
		return true;
	}

	return {
		authed,
		userfromAuth,
		loading,
		login,
		signout,
		getAuth,
		checkSessionData,
	};
}

const formatUser = (user) => {
	return {
		uid: user.session_id,
		email: user.email,
		fname: user.name,
	};
};
