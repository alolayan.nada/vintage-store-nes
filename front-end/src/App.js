import './App.css';
import React, { useState } from 'react';
import Header from './components/Header';
import ProductList from './routes/ProductList';
import Dashboard from './routes/Dashboard';
import Home from './routes/Home';
import Login from './routes/Login';
import Footer from './components/Footer';
import SessionInfo from './components/SessionInfo.jsx';
import Register from './routes/Register';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import Check from './routes/Check';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useAuth, AuthProvider } from './auth';
import { Link, useNavigate, Navigate } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route, Switch } from 'react-router-dom';

function RequireAuth({ children }) {
	const { authed, checkSessionData } = useAuth();
	const check = checkSessionData().then((r) => console.log(r, 'is check'));
	return authed === true ? children : <Navigate to="/" replace />;
}

const stripePromise = loadStripe(
	'pk_test_51HyVXJEfjNsjJFAKg436V6ZIFLMwLWy9A7nx9UhxwTkQqfyxl9aXVIsNU0jfCePzTHdLKcKeM6ZSAIQYtLpau5Qd00RE4RFfWR'
);

function App() {
	return (
		<AuthProvider>
			<div className="App">
				<Elements stripe={stripePromise}>
					<Router>
						<div class="main-header">
							<div>
								<SessionInfo />
							</div>

							<div class="main-header">
								<h1>Floppy Discs</h1>
							</div>
						</div>
						<Routes>
							<Route exact path="/" element={<Home />} />
							<Route exact path="/check" element={<Check />} />
							<Route exact path="/login" element={<Login />} />
							<Route exact path="/register" element={<Register />} />
							<Route
								path="/dashboard"
								element={
									<RequireAuth>
										<Dashboard />
									</RequireAuth>
								}
							/>

							<Route exact path="/home" element={<ProductList />} />
						</Routes>

						<Footer />
					</Router>
				</Elements>
			</div>
		</AuthProvider>
	);
}

export default App;
