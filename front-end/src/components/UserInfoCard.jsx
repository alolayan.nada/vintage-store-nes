import StripeConnectAcount from './StripeConnectAcount';
import { useState, navigate } from 'react';

const UserInfoCard = (userInfo) => {
	const { email, first_name, id, last_name, stripe_id, stripe_id_setup_url } = userInfo.userInfo;
	const strip_details_submitted = userInfo.stripeSetupStatus;
	const [showAccountCreation, setShowAccountCreation] = useState(false);

	function handleShowAccountCreation() {
		setShowAccountCreation(true);
	}

	return (
		<div>
			<div className="dashboard-container">
				<div class="nes-container is-dark with-title">
					<p class="title">User Info</p>
					<div class="nes-table-responsive">
						<table class="nes-table is-bordered is-dark tablec">
							<thead>
								<tr>
									<th className="p-text">User name</th>
									<td>
										<h4>{first_name}</h4>
									</td>
								</tr>
								<tr>
									<th className="p-text">User stripe account status</th>
									<td>
										{strip_details_submitted ? (
											'account setup succeful'
										) : (
											<div>
												<h4 style={{ color: 'red' }}>
													Your Stripe Connect account wasn't setup properly!
												</h4>
												<u onClick={handleShowAccountCreation}>Click here to fix it</u>
											</div>
										)}
										{/* <a href={stripe_id_setup_url}>strip setup account</a> */}
									</td>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				{showAccountCreation ? <StripeConnectAcount id={id} /> : ''}
			</div>
		</div>
	);
};

export default UserInfoCard;
