import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import SpinStat from '../routes/SpinStat';
import axios from 'axios';

const StripeConnectAcount = (id) => {
	const navigate = useNavigate();
	const [stripeId, setStripeId] = useState('');
	const [stripeClicked, setStripeDone] = useState(false);
	const [showElement, setShowElement] = useState(true);
	const [showElementdone, setShowElementdone] = useState(false);
	const [clickable, setclickable] = useState(true);
	const [updateSuccess, setupdateSuccess] = useState(false);
	const handleStripeId = (event) => {};
	function createStripeAccount() {
		const url = axios
			.post('/api/createuser')
			.then((res) => {
				window.open(res.data.url, '_blank');
				setStripeId(res.data.account_id);
				setShowElement(false);
				setclickable(false);
				setStripeDone(!stripeClicked);
			})
			.catch((err) => console.log(err));
	}
	const handleSubmit = (e) => {
		axios
			.put(`/api/${id.id}`, { stripeId: stripeId })
			.then((res) => {
				if (res.status === 201) {
					setShowElementdone(true);
					setupdateSuccess(true);
				}
			})
			.catch((err) => {
				console.log('Error registering user', err);
			});
	};
	return (
		<div className="dashboard-container">
			<div class="nes-container is-dark with-title">
				<p class="title">Set up Stripe Account</p>
				<div className="add-game-wrapper">
					{!stripeClicked && (
						<button
							class="nes-btn is-primary"
							onClick={() => {
								createStripeAccount();
								setStripeDone(true);
							}}
						>
							Set up your account
						</button>
					)}
				</div>

				<input
					id="img_field"
					type="text"
					className="nes-input is-dark is-success"
					placeholder="stripe id will be generated here"
					value={stripeId}
				/>

				<div className="add-game-wrapper">
					{!clickable && !showElementdone ? (
						<h3> click submit after completing the registeration in the pop-up window</h3>
					) : (
						''
					)}
					{showElementdone && <h3> Stripe Connect account id updated succefuly!</h3>}
				</div>
				<div>
					{!updateSuccess ? (
						<button disabled={clickable} onClick={handleSubmit}>
							Submit
						</button>
					) : (
						''
					)}
				</div>
				<div className="add-game-wrapper">
					{stripeClicked && showElement ? (
						<div>
							<SpinStat />
							<alert variant="success">Loading Stripe account setup window sitTight!...</alert>
						</div>
					) : (
						''
					)}
				</div>
			</div>
		</div>
	);
};

export default StripeConnectAcount;
