import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { useAuth } from '../auth';

const Header = (props) => {
	const navigate = useNavigate();
	const { signout, authed, getAuth } = useAuth();

	const [inHome, setInHome] = useState(false);
	const [inDashboard, setInDashboard] = useState(false);

	function handleLoginStatus() {
		getAuth().then((res) => console.log(res));
	}

	useEffect(() => {
		handleLoginStatus();
	}, []);

	return (
		<div>
			<div className="login-wrapper">
				<>
					{!authed ? (
						<button
							onClick={() => {
								navigate('/login');
							}}
							type="button"
							className="nes-btn is-primary is-dark"
							id="seller-signup-btn"
						>
							Login{' '}
						</button>
					) : (
						''
					)}
				</>

				<>
					{authed ? (
						!inDashboard ? (
							<button
								onClick={() => {
									setInDashboard(false);

									navigate('/dashboard');
								}}
								type="button"
								className="nes-btn is-primary is-dark"
								id="seller-signup-btn"
							>
								Dashboard{' '}
							</button>
						) : (
							''
						)
					) : (
						''
					)}
				</>

				<button
					onClick={() => {
						navigate('/');
						setInHome(!inHome);
						props.showHomefunc();
					}}
					type="button"
					className="nes-btn is-primary is-dark"
					id="seller-login-btn"
				>
					Home{' '}
				</button>
			</div>

			<div className="header">
				<>
					{!authed ? (
						<button
							onClick={() => {
								props.showRegisterfunc();
							}}
							type="button"
							className="nes-btn is-primary is-dark"
							id="seller-signup-btn"
						>
							SignUp{' '}
						</button>
					) : (
						''
					)}

					{authed ? (
						<button
							onClick={() => signout().then(() => navigate('/'))}
							type="button"
							className="nes-btn is-primary is-dark"
							id="seller-login-btn"
						>
							Logout{' '}
						</button>
					) : (
						''
					)}
				</>
			</div>
		</div>
	);
};

export default Header;
