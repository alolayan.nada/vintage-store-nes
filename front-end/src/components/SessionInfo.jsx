import { useState, useEffect } from 'react';

import axios from 'axios';

const SessionInfo = () => {
	const [loginData, setLoginData] = useState();
	const [sessionData, setSessionData] = useState();
	const [logoutData, setLogoutData] = useState();
	const [expiryDate, setexpiryDate] = useState();
	const [intitialDate, setintitialDate] = useState();
	const [name, setName] = useState();
	const [email, setEmail] = useState();
	const [sessionId, setsessionId] = useState();
	const [valid, setValid] = useState();
	const [isLoading, setisLoading] = useState(true);
	const [reStatus, setresStatus] = useState(false);

	const refreshTime = 3000; //How frequently you want to refresh the data, in ms

	const fetchSession = async () => {
		setisLoading(true); //set to true only when the api call is going to happen

		axios
			.post('/api/sessiong')
			.then((res) => {
				if (res.data) {
					setresStatus(true);
					setSessionData(res.data.user);
					setexpiryDate(res.data.exp);
					setintitialDate(res.data.iat);
					setName(res.data.user.name);
					setEmail(res.data.user.email);
					setValid(res.data.user.valid);
					setsessionId(res.data.user.session_id);
				}
			})
			.catch((error) => {
				const errormsg =
					'No valid Access Token. PLEASE LOG-IN  in to get an access token from your refresh token associated with your account';
				setSessionData(errormsg);
				setresStatus(false);
			});

		setisLoading(false); //make sure to set it to false so the component is not in constant loading state
	};

	useEffect(() => {
		const comInterval = setInterval(fetchSession, refreshTime); //This will refresh the data at regularIntervals of refreshTime
		return () => clearInterval(comInterval); //Clear interval on component unmount to avoid memory leak
	}, []);

	return (
		<div className="App">
			<section className="nes-container">
				<section className="nes-container is-dark">
					<section className="message-list">
						<section className="message">
							<div className="nes-balloon from-left is-dark">
								<h3>Your session info</h3>
								{isLoading ? (
									<h1>Loading</h1>
								) : (
									<div>
										<div className="nes-table-responsive">
											<table className="nes-table  is-dark is-centered center">
												<thead>
													<tr>
														<div>
															{isLoading ? (
																<h1>Loading</h1>
															) : (
																<div>{JSON.stringify(sessionData, null, 4)}</div>
															)}
														</div>
													</tr>
												</thead>
											</table>
										</div>
										<div>
											{reStatus ? (
												<div className="nes-table-responsive">
													<table className="nes-table is-bordered is-dark is-centered ">
														<thead>
															<tr>
																<th>Access token life span</th>
																<th>JWT initiation</th>
																<th>JWT expiry</th>
																<th>Resfresh token life span </th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>5 seconds</td>
																<td>{intitialDate}</td>
																<td>{expiryDate}</td>
																<td>1 year </td>
															</tr>
														</tbody>
													</table>
												</div>
											) : (
												<section className="message-list">
													<section className="message">
														<div className="nes-balloon  is-dark">
															test with <p>email:c@gmail.com</p> <p>pass:1234</p> stripe seller test
															account set
														</div>
														<div className="nes-balloon  is-dark">
															test with <p>email:agu@gmail.com</p> <p>pass:1234</p> stripe seller
															test account not set
														</div>
													</section>
												</section>
											)}
										</div>
									</div>
								)}
							</div>
							<i className="nes-octocat animate right"></i>
						</section>
					</section>
				</section>
			</section>
		</div>
	);
};

export default SessionInfo;
