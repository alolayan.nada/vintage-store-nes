import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import ImageUpload from './ImageUpload';

import axios from 'axios';
import { Image, Box } from '@chakra-ui/react';

const AddGame = (sessionData) => {
	const [cover, setCover] = useState('');
	const navigate = useNavigate();
	const [title, setTitle] = useState();
	const [price, setPrice] = useState();
	const [imageUrl, setImageUrl] = useState();
	const [condition, setCondition] = useState();
	const [sellerid, setSellerId] = useState(sessionData.sessionData.sessionData.email);
	const [stripeId, setStripeId] = useState('');
	const [stripeurl, setStripeUrl] = useState(''); //stripe account id for seller to get paid

	function fetchUserInfo() {
		const user = axios.post('/getUserInfo', { email: sellerid }).then((res) => {
			setStripeId(res.data[0].stripe_id);
		});
	}
	useEffect(() => {
		fetchUserInfo();
	}, []);

	const handleTitle = (event) => {
		setTitle(event.target.value);
	};
	const handlePrice = (event) => {
		setPrice(event.target.value);
	};
	const handleImageUrl = (event) => {
		setImageUrl(event.target.value);
	};
	const handleCondition = (event) => {
		setCondition(event.target.value);
	};
	const handlecover = async (cover) => {
		setCover(cover);
	};

	const postItem = (email) => {
		axios
			.post('/itemList', {
				image_URL: cover,
				game_price: price,
				game_title: title,
				game_condition: condition,
				seller_name: sellerid,
				seller_stripe_id: stripeId,
			})
			.then((res) => {
				navigate('/');
			});
	};

	return (
		<div className="login-container">
			<div>
				<form onSubmit={() => postItem(sessionData.email)}>
					<div className="add-game-wrapper">
						<label htmlFor="title_field">Game Title</label>
						<input
							id="name_field"
							type="text"
							className="nes-input is-dark"
							placeholder="Game Title"
							onChange={handleTitle}
						/>

						<label htmlFor="price_field">Price</label>
						<input
							id="price_field"
							type="text"
							className="nes-input is-dark "
							placeholder="Price"
							onChange={handlePrice}
						/>
						<label htmlFor="img_field">Upload your game cover</label>

						<Box boxSize="sm">
							{cover ? <Image src={cover} alt="no game cover uploaded" /> : ''}
						</Box>
						<ImageUpload geturl={handlecover} />

						<div className="nes-select is-dark">
							<label htmlFor="dark_select">Condition of Game</label>
							<select required id="dark_select" onChange={handleCondition}>
								<option value="" disabled selected hidden>
									Select Condition
								</option>
								<option value="S">S</option>
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
								<option value="D">D</option>
							</select>
						</div>
						<label htmlFor="img_field">stripe id</label>
						<input
							id="img_field"
							type="text"
							className="nes-input is-dark is-success"
							placeholder="stripe id"
							value={stripeId}
						/>
					</div>
					<div className="add-game-wrapper">
						<button type="submit" className="nes-btn ">
							<h3>Submit</h3>
						</button>
					</div>
				</form>
			</div>
		</div>
	);
};

export default AddGame;
