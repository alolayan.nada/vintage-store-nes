import axios from 'axios';
import React, { useState } from 'react';
import AddGame from './AddGame';
import { useEffect } from 'react';
import Product from './Product';
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry';

const Profile = (sessionData) => {
	const [inputState, setInputState] = useState(false);
	const [ownItems, setOwnItems] = useState([]);
	const [addGame, setaddGame] = useState(true);

	const handleInputState = () => {
		setInputState((state) => !state);
	};

	useEffect(() => {
		async function getSessionData() {
			axios.post('/api/getOwnItems').then((res) => setOwnItems(res.data));
		}
		getSessionData();
	}, []);

	return (
		<div>
			<section className="dashboard-container">
				<div>
					{addGame ? (
						<button
							type="button"
							className="nes-btn is-primary"
							onClick={() => {
								handleInputState();
								setaddGame(!addGame);
							}}
						>
							Add a game
						</button>
					) : (
						<button
							type="button"
							className="nes-btn is-primary"
							onClick={() => {
								handleInputState();
								setaddGame(!addGame);
							}}
						>
							My games
						</button>
					)}
				</div>

				{inputState && <AddGame sessionData={sessionData} />}

				<div>
					{!inputState ? (
						<ResponsiveMasonry columnsCountBreakPoints={{ 350: 1, 600: 2, 900: 3 }}>
							<Masonry>
								{ownItems &&
									ownItems.map((product) => <Product product={product} key={product.id} />)}
							</Masonry>
						</ResponsiveMasonry>
					) : (
						''
					)}
				</div>
			</section>
		</div>
	);
};

export default Profile;
