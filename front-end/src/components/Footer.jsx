import axios from 'axios';
const Footer = () => {
	return (
		<div>
			<div className="social-buttons">
				<div class="nes-container is-dark ">
					<a href="">
						<i class="nes-icon gmail is-medium"></i>
					</a>
					<a href="">
						<i class="nes-icon github is-medium"></i>
					</a>
					<a href="">
						<i class="nes-icon linkedin is-medium"></i>
					</a>
					<div id="footer">&copy; All rights reserved</div>
				</div>
			</div>
		</div>
	);
};

export default Footer;
