import { storage } from '../firebaseConfig';
import { useState } from 'react';
import { Progress } from '@chakra-ui/react';

const ImageUpload = (props) => {
	const [image, setImage] = useState('');
	const [url, setUrl] = useState('');
	const [progress, setProgress] = useState(0);
	const [error, setError] = useState('');

	function onInputChange(e) {
		const file = e.target.files[0];

		if (file) {
			const fileType = file.type;
			const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
			if (validImageTypes.includes(fileType)) {
				handleUpdate(file);
			} else {
				setError('please upload a file');
			}
		}
	}

	function handleUpdate(image) {
		if (image) {
			const uploadTask = storage.ref(`images/${image.name}`).put(image);

			uploadTask.on(
				'state_changed',
				(snapshot) => {
					const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
					setProgress(progress);
				},
				(error) => {
					console.log(error);
					setError(error);
				},
				() => {
					storage
						.ref('images')
						.child(image.name) // Upload the file and metadata
						.getDownloadURL() // get download url
						.then((url) => {
							props.geturl(url);

							setUrl(url);

							setProgress(0);
							setImage(image);
						});
				}
			);
		} else {
			setError('Error please choose an image to upload');
		}
	}

	return (
		<div>
			<div>
				<label className="custom-file-upload">
					<input type="file" onChange={onInputChange} />
				</label>
			</div>

			<div style={{ height: '50px' }}>
				{progress > 0 ? (
					<Progress size="xs" isIndeterminate>
						{' '}
						please wait for the upload then submit
					</Progress>
				) : (
					''
				)}
				<p style={{ color: 'red' }}>{error}</p>
			</div>
		</div>
	);
};

export default ImageUpload;
