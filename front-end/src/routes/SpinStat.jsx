import React from 'react';
import CircularProgress from '@mui/material/CircularProgress';

const SpinStat = () => {
	return (
		<div>
			<CircularProgress />
		</div>
	);
};

export default SpinStat;
