import React, { useState, useEffect } from 'react';
import Product from '../components/Product';
import SearchBar from '../components/SearchBar';
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry';

import axios from 'axios';

const ProductList = () => {
	const [products, setProducts] = useState([]);
	const [filteredProducts, setFilteredProducts] = useState();

	function getProducts() {
		axios
			.post('/itemListg', { withCredentials: true })
			.then((res) => {
				setProducts(res.data);
				setFilteredProducts(res.data);
			})
			.catch((err) => console.log(err));
	}

	useEffect(() => {
		getProducts();
	}, []);
	//
	return (
		<div className="search-bar-container">
			<SearchBar
				products={products}
				filteredProducts={filteredProducts}
				setFilteredProducts={setFilteredProducts}
				setProducts={setProducts}
			/>

			<ResponsiveMasonry columnsCountBreakPoints={{ 350: 1, 600: 2, 900: 3 }}>
				<Masonry>
					{filteredProducts &&
						filteredProducts.map((product) => <Product product={product} key={product.id} />)}
				</Masonry>
			</ResponsiveMasonry>
		</div>
	);
};

export default ProductList;
