import Header from '../components/Header';
import Login from './Login';
import Register from './Register';

import ProductList from './ProductList';
import { useState } from 'react';
import axios from 'axios';

const Home = () => {
	const [showLogin, setShowLogin] = useState(false);
	const [showRegister, setShowRegister] = useState(false);
	const [showHome, setShowHome] = useState(true);
	const [showDashBoard, setShowDashBoard] = useState(false);
	const [isLogged, setisLogged] = useState(false);
	const [sessionData, setSessionData] = useState('');

	async function getSessionData() {
		const check = await axios
			.post(`/api/sessiong`, {
				withCredentials: true,
			})
			.then((res) => {
				setSessionData(res.data);

				return res.data;
			})
			.catch((error) => setSessionData(error.message));
	}

	function showLoginfunc() {
		setShowRegister(false);
		setShowHome(false);
		setShowLogin(true);
	}
	function showRegisterfunc() {
		setShowLogin(false);
		setShowHome(false);
		setShowRegister(true);
	}

	function showHomefunc() {
		setShowRegister(false);
		setShowLogin(false);
		setShowHome(true);
	}

	function isLoggedfunc(data) {
		getSessionData();
		setShowRegister(false);
		setShowLogin(false);
		setShowHome(false);
		setisLogged(true);
		setShowDashBoard(true);
	}

	function getSessionfunc(session) {
		setSessionData(session);
	}
	function showDashBoardfunc() {
		setShowRegister(false);
		setShowLogin(false);
		setShowHome(false);
		setShowDashBoard(!showHome);
	}

	return (
		<div>
			<div className="App">
				<Header
					showLoginfunc={showLoginfunc}
					showRegisterfunc={showRegisterfunc}
					showHomefunc={showHomefunc}
					isLogged={isLogged}
					getSession={getSessionfunc}
					sessionInfo={sessionData}
				/>
			</div>

			<div>{showHome && <ProductList />}</div>

			<div>
				{showLogin ? (
					<Login isLoggedfunc={isLoggedfunc} showDashBoardfunc={showDashBoardfunc} />
				) : null}
			</div>
			<div>{showRegister ? <Register /> : null}</div>
		</div>
	);
};
export default Home;
