import React, { useState } from 'react';

import { useNavigate, useLocation } from 'react-router-dom';

import { useAuth } from '../auth';

const Login = () => {
	const { loginStatus } = useLocation();

	const { login, userfromAuth, loading, authed } = useAuth();

	const navigate = useNavigate();
	const [loggedIn, setLoggedIn] = useState(false);
	const [authResult, setAuthResult] = useState('');

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// Login form field handlers
	function handleEmailChange(e) {
		setEmail(e.target.value);
	}
	function handlePasswordChange(e) {
		setPassword(e.target.value);
	}

	const handleSignUp = () => {
		navigate('/');
	};

	//on Register form submit
	const handleSubmit = (e) => {
		e.preventDefault();
		login(email, password).then((res) => {
			if (res) {
				setLoggedIn(true);
				navigate('/dashboard');
			} else {
				setLoggedIn(false);
				setAuthResult('Either Email or password are incorrect!');
			}
		});
	};

	return (
		<div className="login-container">
			<div class="nes-container is-dark ">
				<div className="login-wrapper">
					<form onSubmit={handleSubmit}>
						<label htmlFor="email_field">Enter your email</label>

						<input
							id="email_field"
							className="nes-input is-dark"
							placeholder="Email"
							required
							type="email"
							value={email}
							onChange={(e) => handleEmailChange(e)}
						/>
						<label htmlFor="password_field">Enter your password</label>

						<input
							id="password_field"
							type="password"
							className="nes-input is-dark"
							placeholder="Password"
							required
							value={password}
							onChange={(e) => handlePasswordChange(e)}
						/>
						<button className="nes-btn is-primary is-dark" onClick={handleSignUp}>
							Home
						</button>
						<button className="nes-btn  is-dark" type={'submit'}>
							Log In
						</button>
					</form>
				</div>
				<div>{authResult}</div>
			</div>
		</div>
	);
};

export default Login;
