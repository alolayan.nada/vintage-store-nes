import React from 'react';
import Profile from '../components/Profile';
import Header from '../components/Header';
import UserInfoCard from '../components/UserInfoCard';
import { useEffect, useState } from 'react';

import axios from 'axios';

const Dashboard = (email) => {
	const [sessionData, setSessionData] = useState('');
	const [userInfo, setUserInfo] = useState('');
	const [stripeSetupStatus, setStripeSetUpStatus] = useState(false);

	async function logAccountObject(stripe_id) {
		await axios.post('/api/stripeAccount', { account_id: stripe_id }).then((res) => {
			setStripeSetUpStatus(res.data.details_submitted);
		});
	}

	async function getSessionData() {
		await axios
			.post(`/api/sessiong`, {
				withCredentials: true,
			})
			.then((res) => {
				setSessionData(res.data.user);

				getUserInfo(res.data.user.email);
			})
			.catch((error) => setSessionData(error.message));
	}

	async function getUserInfo(email) {
		await axios
			.post('/getUserInfo', {
				email: email,
			})
			.then((res) => {
				setUserInfo(res.data[0]);

				logAccountObject(res.data[0].stripe_id);
			});
	}

	useEffect(() => {
		getSessionData();
	}, []);

	return (
		<div>
			<div className="dashboard-container">
				<Header />

				<div className="nes-container is-dark ">
					<UserInfoCard userInfo={userInfo} stripeSetupStatus={stripeSetupStatus} />
					<div className="nes-container is-dark with-title">
						<p class="title">Game List</p>
						<div>
							<Profile sessionData={sessionData} />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Dashboard;
