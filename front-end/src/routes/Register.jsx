import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import SpinStat from './SpinStat';
import axios from 'axios';

const generateName = require('sillyname');
const gName = generateName();

const Register = () => {
	const navigate = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState(' ');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [stripeId, setStripeId] = useState('');
	const [loginSuccess, setLoginSuccess] = useState(false);
	const [showfields, setShowFields] = useState(true);
	const [stripeClicked, setStripeDone] = useState(false);
	const [stripeurl, setStripeUrl] = useState(''); //stripe account id for seller to get paid
	const [clickable, setclickable] = useState(true);
	const [showElement, setShowElement] = useState(true);

	const handleLogIn = () => {
		navigate('/login', { replace: true, state: { email: 'email' } });
	};

	useEffect(() => {
		setTimeout(function () {
			setShowElement(false);
		}, 7000);
	}, []);

	function createStripeAccount() {
		const url = axios
			.post('/api/createuser')
			.then((res) => {
				window.open(res.data.url, '_blank');
				setStripeId(res.data.account_id);
				setStripeUrl(res.data.url);
				setclickable(false);
				setStripeDone(!stripeClicked);
			})
			.catch((err) => console.log(err));

		//
	}

	const handleStripeId = (event) => {};
	// Form Field Handlers
	function handleFirstNameChange(e) {
		setFirstName(e.target.value);
	}
	function handleLastNameChange(e) {
		setLastName(e.target.value);
	}
	function handleEmailChange(e) {
		setEmail(e.target.value);
	}
	function handlePasswordChange(e) {
		setPassword(e.target.value);
	}
	function handleAutoFill() {
		setFirstName(gName);
		const firstNameg = gName.split(' ').slice(0, -1).join(' ');
		setEmail(firstNameg + '@gmail.com');
	}

	function resetForm() {
		setFirstName('');
		setLastName('');
		setEmail('');
		setPassword('');
	}

	//on Register form submit
	const handleSubmit = (e) => {
		e.preventDefault();

		axios
			.post('/signup', {
				first_name: firstName,
				last_name: lastName,
				email: email,
				password: password,
				stripe_id: stripeId,
				stripe_id_setup_url: stripeurl,
			})
			.then((res) => {
				console.log('Login Success', res);
				if (res.status === 201) {
					//Reset Form field
					resetForm();
					// Inform User
					setLoginSuccess(true);

					return navigate('/login');
				}
			})
			.catch((err) => {
				console.log('Error registering user', err);
			});
	};

	return (
		<div className="login-container">
			<div class="nes-container is-dark ">
				<h4>Register as a Seller and start selling on our platforms right away</h4>
				<button className="label" className="nes-btn  is-dark" onClick={handleAutoFill}>
					want to test the flow? how about Autofill registeration? using test values for test keys.
				</button>
				{showfields && (
					<div className="login-wrapper">
						<form onSubmit={handleSubmit}>
							<label htmlFor="first_name_field" className="label">
								Name
							</label>

							<input
								id="first_name_field"
								type="text"
								className="nes-input is-dark"
								placeholder="First Name"
								required
								value={firstName}
								onChange={(e) => handleFirstNameChange(e)}
							/>

							<label htmlFor="last_name_field">Email</label>

							<input
								id="email_field"
								type="email"
								className="nes-input is-dark"
								placeholder="Email"
								required
								value={email}
								onChange={(e) => handleEmailChange(e)}
							/>
							<label htmlFor="password_field">Password</label>

							<input
								id="password_field"
								type="password"
								className="nes-input is-dark"
								placeholder="Password"
								required
								value={password}
								onChange={(e) => handlePasswordChange(e)}
							/>
							<label htmlFor="stripe_id"></label>
							<input
								id="img_field"
								type="text"
								className="nes-input is-dark is-success"
								placeholder="stripe id"
								value={stripeId}
								onChange={handleStripeId}
							/>

							<div>
								{!stripeClicked && (
									<h3>set up your stripe account to get paid through our platform!</h3>
								)}
							</div>

							<div className="add-game-wrapper">
								{!stripeClicked && (
									<button
										onClick={() => {
											createStripeAccount();
											setStripeDone(true);
										}}
									>
										<div>{!stripeClicked && <h3>generate stripe id</h3>}</div>
									</button>
								)}
							</div>

							<div className="add-game-wrapper">
								{stripeClicked && showElement ? (
									<div>
										<SpinStat />
										<alert variant="success">Loading Stripe id sitTight!...</alert>
									</div>
								) : (
									''
								)}
							</div>
							<div className="add-game-wrapper">
								{!clickable && <h3>You can go ahead and register now! </h3>}
							</div>
							<div>{!loginSuccess ? <button disabled={clickable}>Sign Up</button> : ''}</div>

							<div>
								{loginSuccess ? (
									<button className="nes-btn  is-dark" onClick={handleLogIn}>
										Login
									</button>
								) : (
									''
								)}
							</div>
						</form>
					</div>
				)}

				{loginSuccess && <p> Registered Successfully, Login to continue</p>}
			</div>
		</div>
	);
};

export default Register;
